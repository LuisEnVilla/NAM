// Controlador backend Principal
var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function (req, res, next) {
	res.render('index');
});

/* POST LOG. Si existe el usuario crea la sesion */
router.post('/', function (req, res, next) {
	connection.query('SELECT * FROM users WHERE mail= "' + req.body.mail + '"', function (err, rows, fields) {
		if (err) throw err;
		if (req.body.pw == rows[0].pw) {
			req.session.pw = rows[0].pw;
			req.session.name = rows[0].name;
			req.session.mail = rows[0].mail;
			res.jsonp({
				"log": rows[0].name
			});
		} else {
			res.status(404).jsonp({
				"log": "FAIL"
			});
		}
	});
});

/* GET LOGOUT Termina la sesion y redirigue a index*/
router.get('/logout', function (req, res, next) {
	req.session.destroy(function (err) {
		if (err) console.log(err);
		else res.redirect('/');
	})
});


/* GET NEW USERS page. Rednderiza la vista para crear un USER */
router.get('/new', function (req, res, next) {
	res.render('newUser');
});

/* GET DASH page.  Renderiza la vista de DASHBOARD cuando este una sesion iniciada, si no redirigue a login*/
router.get('/dashboard', function (req, res, next) {
	if (req.session.name) {
		res.render('dashboard');
	} else {
		res.redirect("/");
	}
});

/*GET ALL CONTRY Consulta los paises de la BD y regresa todos ordenados por nombre*/
router.get('/country', function (req, res, next) {
	connection.query('SELECT * FROM country ORDER BY nombre', function (err, rows, fields) {
		if (err) throw err;
		res.jsonp(rows);
	});
});

module.exports = router;
/*Controlador APIREST USERS*/
var express = require('express');
var router = express.Router();

/* GET ALL USERS: Regresa todos los usuarios*/
router.get('/', function (req, res, next) {
	connection.query('SELECT * FROM users', function (err, rows, fields) {
		if (err) throw err;
		res.jsonp(rows);
	});
});

/*GET SESSION: Regresa los datos de la sesion actual*/
router.get('/session/', function (req, res, next) {
	res.jsonp(req.session);
});

/*INSERT ONE USER: Crea un registro en USERS y una sesion con los mismos datos*/
router.post('/', function (req, res, next) {
	var name = req.body.name
	var mail = req.body.mail
	var pw = req.body.pw
	var query = 'INSERT INTO users (name, mail, pw) VALUES ("' + name + '","' + mail + '","' + pw + '")'
	connection.query(query, function (err, result) {
		if (err) throw err;
		req.session.pw = req.body.pw;
		req.session.name = req.body.name;
		req.session.mail = req.body.mail;
		res.redirect("/dashboard");
	});
});

/*UPDATE ONE USERS: Actualiza los datos de un registro y los datos de la sesion actual*/
router.put('/', function (req, res, next) {
	var name = req.body.name
	var mail = req.body.mail
	var pw = req.body.pw
	var query = 'UPDATE users SET name= "' + name + '", mail="' + mail + '", pw= "' + pw + '" WHERE mail="' + req.session.mail + '"'
	connection.query(query, function (err, result) {
		if (err) throw err;
		req.session.pw = req.body.pw;
		req.session.name = req.body.name;
		req.session.mail = req.body.mail;
		res.jsonp({
			"update": req.session.mail
		});
	});
});

/*DELETE ONE USERS: Elimina el registro de USERS de la sesion actual y cierra su sesion*/
router.get('/delete', function (req, res, next) {
	var query = 'DELETE FROM users WHERE mail="' + req.session.mail + '"'
	connection.query(query, function (err, result) {
		if (err) throw err;
		res.redirect("/logout");
	});
});

module.exports = router;
/*Controlador APIREST PEOPLE*/
var express = require('express');
var router = express.Router();
var PDF = require('pdfkit');
var fs = require('fs');

/* GET ALL PEOPLE: consulta los registros de PEOPLE los ordena por pais y los regresa*/
router.get('/', function (req, res, next) {
	connection.query('SELECT * FROM people ORDER BY contry', function (err, rows, fields) {
		if (err) throw err;
		res.jsonp(rows);
	});
});

/*FIND FOR ID: Busca un registro de PEOPLE por su ID y lo retorna*/
router.get('/find/:id', function (req, res) {
	connection.query('SELECT * FROM people WHERE IdPeople = ' + req.params.id, function (err, rows, fields) {
		if (err) throw err;
		res.jsonp(rows[0]);
	});
});

/*INSERT PEOPLE: Inserta un nuevo registro a PEOPLE*/
router.post('/', function (req, res, next) {
	var name = req.body.name
	var contry = req.body.contry
	var photo = req.body.photo
	var query = 'INSERT INTO people (name, contry, photo) VALUES ("' + name + '","' + contry + '","' + photo + '")'
	connection.query(query, function (err, result) {
		if (err) throw err;
		res.jsonp({
			"insert": name
		});
	});
});

/*CREATE PDF: Crea un PDF con los regirtros de PEOPLE y lo visualiza en el navegador. 
Cuando tiene una sesion iniciada, de lo contrario redirigue a LOGIN*/
router.get('/pdf', function (req, res) {
	if (req.session.name) {
		connection.query('SELECT * FROM people ORDER BY contry', function (err, rows, fields) {
			if (err) throw err;
			report(rows).pipe(res);
		});
	} else {
		res.redirect("/");
	}
});

/*UPDATE PEOPLE FOR ID: 

	* GET : recupero el registro por su ID y renderiza la vista de EDITPEOPLE. 
			Si no tiene sesion redirigue a LOGIN
	* PUT : Actualiza un registro por su ID
*/
router.get('/:id', function (req, res, next) {
	if (req.session.name) {
		connection.query('SELECT * FROM people WHERE IdPeople=' + req.params.id, function (err, rows, fields) {
			if (err) throw err;
			res.render("editPeople", {
				people: rows[0]
			});
		});
	} else {
		res.redirect("/");
	}
});


router.put('/:id', function (req, res, next) {
	var name = req.body.name
	var contry = req.body.contry
	var query = 'UPDATE people SET name= "' + name + '", contry="' + contry + '" WHERE IdPeople=' + req.params.id
	connection.query(query, function (err, result) {
		if (err) throw err;
		res.jsonp({
			"update": req.params.id
		});
	});
});

/*DELETE ONE PEOPLE: 

 	*DELETE: Elimina un regirtro de PEOPLE por su ID
 	*GET: Elimina un registro de PEOPLE por su ID y redirigue a DASHBOARD
 */
router.delete('/:id', function (req, res, next) {
	var query = 'DELETE FROM people WHERE IdPeople=' + req.params.id
	connection.query(query, function (err, result) {
		if (err) throw err;
		res.jsonp({
			"delete": req.params.id
		});
	});
});

router.get('/delete/:id', function (req, res, next) {
	var query = 'DELETE FROM people WHERE IdPeople=' + req.params.id
	connection.query(query, function (err, result) {
		if (err) throw err;
		res.redirect("/dashboard");
	});
});

/*Crea el PDF con pdfkit, recibiendo el arreglo de registros de PEOPLE*/
function report(people) {
	var path = "/home/"
	doc = new PDF();
	doc.pipe(fs.createWriteStream(path + 'report.pdf'));
	// TITULO
	doc.fontSize(18);
	doc.text('REPORTE DE PERSONAS', {
		align: "center"
	});
	// FECHA
	doc.fontSize(12);
	var date = new Date();
	doc.text(date.toDateString(), {
		align: "center"
	});
	//CABESERAS DE LA TABLA
	doc.moveDown(2);
	doc.fillColor('blue').text("Nombre\nPaís\nImagen\n ", {
		columns: 3,
		height: 14,
		align: 'center'
	})
	doc.fillColor('black').text(" ");
	for (var i = 0; i < people.length; i++) {
		//NOMBRE Y PAIS
		doc.text(people[i].name + "\n" + people[i].contry + "\n ", {
			columns: 3,
			columnGap: 15,
			height: 14,
			align: 'center',
			continued: true
		});
		// LINK PHOTO
		doc.text("Imagen Link", {
			link: people[i].photo,
			underline: true,
			height: 14,
			align: 'center',
			columnGap: 15
		});
	}
	doc.end();
	return doc;
}

module.exports = router;
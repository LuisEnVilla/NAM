// connection BD 
/*:::::::::::::::NOTA::::::::::
	Para que todo funcione bien debes crear las siguientes tablas en tu base de MySQL en una BD llamada "nam"

	- users
		* IdUsers: int(10),AUTO_INCREMENT,unico
		* name: varchar(30)
		* mail: varchar(30)
		* pw: varchar(10)
	
	- people
		* IdPeople: int(10),AUTO_INCREMENT,unico
		* name: varchar(30)
		* contry: varchar(30)
		* photo: varchar(200)
	
	- country
		* nombre: varchar(45)
		*** Adjunte "paises.csv" la puedes importar  a tu BD
*/
var mysql      = require('mysql');
global.connection = mysql.createConnection({
  host     : 'localhost',
  user     : 'root',
  password : '',
  database : 'nam',
});

module.exports = global.connection;
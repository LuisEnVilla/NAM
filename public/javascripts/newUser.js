// Controlador para la vista newUser
(function () {

	'use strict';

	angular
		.module('formlyApp')
		.controller('newUser', newUser);

	function newUser($http) {

		var vm = this;

		// The model object that we reference
		vm.logIn = {}; //Modelo de USER

		// funcation assignment
		vm.onSubmit = onSubmit;

		// An array of our form fields with configuration
		// the 'fields' attribute on the  element
		// FIELDS para crear USER
		vm.logInFields = [
			{
				key: 'name',
				type: 'input',
				templateOptions: {
					type: 'text',
					label: 'Name',
					placeholder: 'Enter your name',
					required: true
				}
        },
			{
				key: 'mail',
				type: 'input',
				templateOptions: {
					type: 'email',
					label: 'Email',
					placeholder: 'Enter email',
					required: true
				}
        },
			{
				key: 'pw',
				type: 'input',
				templateOptions: {
					type: 'password',
					label: 'Password',
					placeholder: 'Enter your password',
					required: true
				}
        }
    ];

		// Crea el usuario y sesion con los datos del formulario, redirigue a DASHBOARD
		function onSubmit() {
			$http.post('/users/', vm.logIn).
			success(function (data, status, headers, config) {
				window.location = "/dashboard"
			}).
			error(function (data, status, headers, config) {
				alert("Error al crear Usuario.");
			});
		}
	}

})();
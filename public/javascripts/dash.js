/*Controlador para la vista DASHBOARD*/
(function () {

	'use strict';

	angular
		.module('formlyApp')
		.controller('dash', dash);

	function dash($http) {

		var vm = this;

		// The model object that we reference
		vm.logIn = {}; // Modelo para insertar a PEOPLE
		vm.editSession = {}; // Modelo para editar USER
		vm.people = []; // Modelo para mostrar la tabla de PEOPLE

		// Funcion para recuperar los datos de la session actual
		$http.get('/users/session').
		success(function (data, status, headers, config) {
			vm.editSession = data;
		}).
		error(function (data, status, headers, config) {
			alert("error de conexión");
		});

		// Funcion para recuperar la lista completa de PEOPLE
		$http.get('/people').
		success(function (data, status, headers, config) {
			vm.people = data;
		}).
		error(function (data, status, headers, config) {
			alert("error de conexión");
		});

		// funcation assignment
		vm.onSubmit = onSubmit;
		vm.edit = onEditSession;
		vm.deleteUser = deleteUser;

		// An array of our form fields with configuration
		// the 'fields' attribute on the  element

		// FIELDS para insertar en PEOPLE
		vm.logInFields = [
			{
				key: 'name',
				type: 'input',
				templateOptions: {
					type: 'text',
					label: 'Name',
					placeholder: 'Enter your name',
					required: true
				}
            },
			{
				key: 'contry',
				type: 'select',
				templateOptions: {
					label: 'Contry',
					options: getProvinces(),
					required: true
				}
            },
			{
				key: 'photo',
				type: 'input',
				templateOptions: {
					type: 'url',
					label: 'Photo',
					placeholder: 'Enter your url',
					required: true
				}
            }
        ];

		//FIELDS para actualizar USER
		vm.editSessionFields = [
			{
				key: 'name',
				type: 'input',
				templateOptions: {
					type: 'text',
					label: 'New Name',
					placeholder: 'Enter your name',
					required: true
				}
            },
			{
				key: 'mail',
				type: 'input',
				templateOptions: {
					type: 'email',
					label: 'New Email',
					placeholder: 'Enter email',
					required: true
				}
            },
			{
				key: 'pw',
				type: 'input',
				templateOptions: {
					type: 'password',
					label: 'New Password',
					placeholder: 'Enter your password',
					required: true
				}
            }
        ];


		// Inserta en modelo recuperado del formulario a PEOPLE y actualiza la tabla
		function onSubmit() {
			$http.post('/people/', vm.logIn)
			$http.get('/people').
			success(function (data, status, headers, config) {
				vm.people = data;
			}).
			error(function (data, status, headers, config) {
				alert("error de conexión");
			});
			vm.logIn = {};
		}

		// Actualiza el USER actual
		function onEditSession() {
			$http.put('/users/', vm.editSession).
			success(function (data, status, headers, config) {
				alert("Usuario Editado");
			}).
			error(function (data, status, headers, config) {
				alert("error de conexión");
			});
		}

		// Elimina el usuario actual y cierra su sesion 
		function deleteUser() {
			$http.delete('/users/', vm.editSession).
			success(function (data, status, headers, config) {
				window.location = "/logout"
			})
		}

		// Cosnsulta la lista de paises y crea la lista del select country
		function getProvinces() {
			var countryArray = []
			$http.get("/country").success(function (data) {
				for (var x in data) {
					countryArray.push({
						"name": data[x].nombre,
						"value": data[x].nombre
					});
				};
			});
			return countryArray
		}
	}
})();
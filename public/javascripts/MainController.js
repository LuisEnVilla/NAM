/*Controlador del Index para Angular-Formly*/
(function () {

	'use strict';

	angular
		.module('formlyApp')
		.controller('MainController', MainController);

	function MainController($http) {

		var vm = this;

		// The model object that we reference
		vm.logIn = {};

		// funcation assignment
		vm.onSubmit = onSubmit;

		// An array of our form fields with configuration
		// the 'fields' attribute on the  element
		vm.logInFields = [
			{
				key: 'mail',
				type: 'input',
				templateOptions: {
					type: 'email',
					label: 'Email',
					placeholder: 'Enter email',
					required: true
				}
            },
			{
				key: 'pw',
				type: 'input',
				templateOptions: {
					type: 'password',
					label: 'Password',
					placeholder: 'Enter your password',
					required: true
				}
            }
        ];

		// LOGEO, crea la sesion y redirigue a DASBOARD
		function onSubmit() {
			$http.post('/', vm.logIn).
			success(function (data, status, headers, config) {
				window.location = "/dashboard"
			}).
			error(function (data, status, headers, config) {
				alert("Datos Incorrectos");
			});
		}
	}
})();
// Controlador para la vista editPeople
(function () {

	'use strict';

	angular
		.module('formlyApp')
		.controller('editPeople', editPeople);

	function editPeople($http) {

		var vm = this;

		// The model object that we reference
		vm.people = {};
		// Recupera el id de PEOPLE
		var id = window.location.pathname.split('/');
		id = id[id.length - 1];
		// Recupera los datos de PEOPLE actual para llenar el formulario
		$http.get("/people/find/" + id).success(function (data) {
			vm.people = data;
		});

		// funcation assignment
		vm.onSubmit = onSubmit;
		// An array of our form fields with configuration
		// the 'fields' attribute on the  element
		// FIELDS para Actualizar PEOPLE
		vm.peopleFields = [
			{
				key: 'name',
				type: 'input',
				templateOptions: {
					type: 'text',
					label: 'Name',
					placeholder: 'Enter your name',
					required: true
				}
        },
			{
				key: 'contry',
				type: 'select',
				templateOptions: {
					label: 'Contry',
					options: getProvinces(),
					required: true
				}
        },
			{
				key: 'photo',
				type: 'input',
				templateOptions: {
					type: 'url',
					label: 'Photo',
					placeholder: 'Enter your url',
					required: true
				}
        }
    ];

		// Recupera el ID y actualiza el registro.
		function onSubmit() {
			var id = window.location.pathname.split('/');
			id = id[id.length - 1];
			$http.put('/people/' + id, vm.people).
			success(function (data, status, headers, config) {
				window.location = "/dashboard"
			}).
			error(function (data, status, headers, config) {
				alert("Datos Incorrectos");
			});
		}

		// Cosnsulta la lista de paises y crea la lista del select country
		function getProvinces() {
			var countryArray = []
			$http.get("/country").success(function (data) {
				for (var x in data) {
					countryArray.push({
						"name": data[x].nombre,
						"value": data[x].nombre
					});
				};
			});
			return countryArray
		}

	}

})();